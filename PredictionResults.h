/*
 * PredictionResultVector.h
 *
 *  Created on: Feb 9, 2016
 *      Author: mellertson
 */

#ifndef PREDICTIONRESULTS_H_
#define PREDICTIONRESULTS_H_

#include <vector>
#include <numeric>
#include <string>
#include <algorithm>
#include <vector>
#include <stdexcept>
#include <functional>
#include <fann.h>

namespace bad_ass {
using namespace std;

class PredictionResult {
public:
	fann_type 	prediction = 0.0f;
	fann_type 	pct_diff_of_actual = 1000000.0f;
	fann_type 	actual = 0.0f;
	string		date = "";
	string		symbol = "";
	string 		date_prediction_made = "";
	fann_type 	mse = 0.0f;
	fann_type	best_mse = 1000000.0f;
	bool 		error = true;
};

class PredictionResultVector: public std::vector<PredictionResult> {
public:
	/*
	 * instance variables
	 */
	PredictionResult p;

	/*
	 * instance methods
	 */
	PredictionResultVector();
	virtual ~PredictionResultVector();
	vector<fann_type> standardDeviationOfPrice(uint num_deviations);
	vector<fann_type> getPredictions();
	PredictionResult getPredictionObject();

};

} /* namespace bad_ass */

#endif /* PREDICTIONRESULTS_H_ */
