/*
 * MovingAverage.h
 *
 *  Created on: Jan 25, 2016
 *      Author: mellertson
 */

#ifndef MOVINGAVERAGE_H_
#define MOVINGAVERAGE_H_

#include <list>

namespace std {

class MovingAverage {
public:
	MovingAverage(int size);
	virtual ~MovingAverage();
	list<float> ma;
	void addValue(float value);
	float getMovingAverage();
};

} /* namespace std */

#endif /* MOVINGAVERAGE_H_ */
