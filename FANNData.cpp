/*
 * FANNData.cpp
 *
 *  Created on: Jan 19, 2016
 *      Author: mellertson
 */

#include "FANNData.h"
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <exception>
#include <cstdio>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string.hpp>

namespace bad_ass {
	using namespace std;


	FANNData::FANNData() {

	}

	FANNData::~FANNData() {

	}

	/**
	 * Transforms a CSV file of 1 desired output and then the inputs to a .DATA format.  The .DATA file is written to 'outfilename'.
	 *
	 * Example of input CSV file:
	 *      desired_output_1, input_1, input_2, input_3
	 *      desired_output_2, input_1, input_2, input_3
	 *      desired_output_3, input_1, input_2, input_3
	 *
	 * @param filename The CSV filename that will be transformed.
	 * @param outfilename The fully qualified path to the .DATA file that will be created.
	 * @param delimeter The delimeter character used in the CSV file 'filename'.
	 * @returns void
	 */
    void FANNData::transformInputFile(const string &filename, const string &outfilename, const string &delimeter) {
        using namespace boost::algorithm;
        string output;
        std::ifstream file(filename);
        std::string line;
        uint rows = 0, cols = 0;
        bool countcols = true;
		stringstream buffer;
		if(!file.good())
			return;

		do {
			getline (file, line);	// read in 1 line at a time
			stringstream ss(line);
			vector<string> values;
			boost::split(values, line, is_any_of(delimeter));
			for (vector<string>::const_iterator value = values.begin()+1; value != values.end(); value++) {
				buffer << *value << " ";
				if(countcols)
					cols++;
			}
			countcols = false;
			buffer << endl << *values.begin() << endl;
			rows++;
		} while ( file.good() && !file.eof() );
		file.close();	// close the input file

		// Write the transformed data to the file as output
		string header = std::to_string(rows-1) + " " + to_string(cols) + " 1";
		ofstream outfile(outfilename);
		outfile << header << endl << buffer.str();
		outfile.close();
	}

    /**
     * Write input values to the filename in a format the fann predictor can use
     *
     * @param filename
     * @param inputs
     * @param inputsCount
     * @param delimeter
     */
	void FANNData::saveInputsToFile(const string &filename, fann_type *inputs, uint inputsCount, string &delimeter) {
		using namespace boost::algorithm;
		std::ifstream file (filename, std::ifstream::in);
		std::string line;

		while( file.good() && !file.eof() )  {
			getline (file, line);	// read in 1 line at a time
			if(line.empty())
				break;
			stringstream ss(line);
			vector<string> values;
			boost::split(values, line, is_any_of(delimeter));
			uint i = 0;
			for (vector<string>::const_iterator value = values.begin(); value != values.end(); value++) {
				if (i >= inputsCount)
					throw "Attempted to store too many inputs into the fann_type array.";
                try {
                    inputs[i] = (fann_type) stof(*value);
                } catch(std::invalid_argument &e) {

                }

				i++;
			}
		}
		file.close();	// close the input file

		return;
	}

	void FANNData::writeFannTypeToFile(const string &filename, fann_type *value) {
		std::ofstream file (filename, std::ofstream::out);
		if(file.is_open())
			file << fixed << setprecision(4) << *value << endl;
		else
			throw "File '" + filename + "' couldn't be opened.";

		file.close();
		return;
	}

	/**
	 *Extract the last N lines from the CSV file, in_filename, and save it to out_filename as a CSV file.
	 *
	 * @param in_filename - The name of the CSV file we're going to extract lines from.
	 * @param out_filename - The name of the CSV we'll create using the extracted lines.
	 * @param n - The number of lines to extract.
	 * @return uint - The number of lines in that were extracted to the output file.
	 */
	uint FANNData::extractTestSetFromInputFile(const string &in_filename, const string &out_filename, uint n) {
		n = getFileLineCount(in_filename) - n;
		string new_filename(in_filename+"new");
		ifstream in_file(in_filename);
		ofstream in_file_new(new_filename);
		ofstream out_file(out_filename);
		string line;
		uint i = 0;
		while(getline(in_file, line)) {
			// TODO: COMPLETE: 1/28/2106 - Finish writing this function.  It needs to store the top N lines into in_file, and store the last N lines into out_file.
			if (i < n) {
				// Store the lines 1 to N in a new input file
				in_file_new << line << endl;
			} else {
				// store lines N to end of file in the output file
				out_file << line << endl;
			}
			i++;
		}
		// Close files
		in_file_new.close();
		out_file.close();

		// rename files and return
		rename(new_filename.c_str(), in_filename.c_str());
		return getFileLineCount(out_filename);
	}

	/**
	 * Counts the number of lines in a file.
	 *
	 * @param filename
	 * @return uint - the number of lines in the file.
	 */
	uint FANNData::getFileLineCount(const string &filename) {
		std::ifstream file (filename);
		std::string line;
		stringstream buffer;
		uint count = 0;
		if(!file.good()) {
			string error = filename+" couldn't be opened!";
			throw error;
		}
		while(getline(file, line)) {
			count++;
		}
		file.close();
		return count;
	}

} // End of bad_ass namespace





























