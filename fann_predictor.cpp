//============================================================================
// Name        : fann_predictor.cpp
// Author      : Mike Ellertson
// Version     :
// Copyright   : This software is copyright protected.
// Description : fann_predictor, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <sstream>
#include <limits>
#include <algorithm>
#include <stdexcept>
#include <fann.h>
#include <floatfann.h>
#include <curl/curl.h>
#include <unistd.h>
#include <string>
#include <cstdio>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include "FANNData.h"
#include "fann_extras.h"
#include "JSON.h"
#include "GlobalVariables.h"
#include "MovingAverage.h"
#include "PredictionResults.h"

namespace bad_ass {

using namespace std;

typedef numeric_limits< double > dbl;

/***************************************************************************************************
 * Global variables
 ***************************************************************************************************/
GlobalVariables g;

vector<string>& getTrainingFiles(const string &dir) {
	vector<string> *files = new vector<string>;

	namespace fs = boost::filesystem;
	fs::path someDir(dir);
	fs::directory_iterator end_iter;

	typedef multimap<time_t, fs::path> result_set_t;
	result_set_t result_set;

	if ( fs::exists(someDir) && fs::is_directory(someDir))
	{
	  for( fs::directory_iterator dir_iter(someDir); dir_iter != end_iter; ++dir_iter )
	  {
	    if (fs::is_regular_file(dir_iter->status()) )
	    {
	    	// // cout << "File found: "+dir_iter->path().string() << endl;
//	      result_set.insert(result_set_t::value_type(fs::last_write_time(dir_iter->path()), *dir_iter));
	    }
	  }
	}

	return *files;
}

size_t curl_write( void *data, size_t size, size_t nmemb, void *ptr) {
    string *output = (string *)ptr;
    *output += (char *)data;
    return size * nmemb;
}

string callWebService(string url) {
    // Get the data from the web-service and return the fann_train_data structure
    CURL *curl;
    CURLcode res;
    string output;

    // Call the web-service, store the returned data in output
    curl = curl_easy_init();
    if(curl) {
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &output);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curl_write);

        // perform the request, res will have the return code
        res = curl_easy_perform(curl);
        // check for errors
        if(res != CURLE_OK) {
            // cout << "curl_easy_perform() failed: " << curl_easy_strerror(res) << "\n";
        }

        // cleanup
        curl_easy_cleanup(curl);
    }

    // cleanup the output data
    size_t i = output.find('}');
    if (i != string::npos) {
        output.resize(i + 3);
    }
    return output;
}

/**************************************************************************
// loadDataSet() makes a call to the web-service using the inputted URL,
// command, start and end dates, time interval, username and password
// and returns the data in the
// Inputs:  string url - in the form of http://...
//          string username - username to validate access
//          string password - password for username
//          string command - OPEN, CLOSE, HIGH, or LOW
//          string interval - time interval in minutes, must be divisible
//                      by 5 (e.g. 5, 10, 15, 30, 60, ...)
//          string startDate - date time value formatted like: 2014-01-19 23:59:59
//          string endDate - date time formatted like startDate
// Returns: vector<fann_type> - populated with a two dimensional array with
//                      time interval and price data sent back from the web-service
// Throws:  runtime_error
**************************************************************************/
vector<fann_type> loadDataSet(string url,
                                   string username, string password,
                                   string command, int interval,
                                   string startDate, string endDate) {
    // Local variables
    vector<fann_type> v_fData; // holds the data returned from the web-service
    string q = "?", amp = "&";

    // Check for required argument
    if (url.size() == 0) throw runtime_error("Argument 'url' was empty, and is required.");
    if(username.size() == 0) throw runtime_error("Argument 'username' was empty, and is required.");
    if(password.size() == 0) throw runtime_error("Argument 'password' was empty, and is required.");
    if(command.size() == 0) throw runtime_error("Argument 'command' was empty, and is required.");
    if(interval == 0) throw runtime_error("Argument 'command' was empty, and is required.");
    if(startDate.size() == 0) throw runtime_error("Argument 'startDate' was empty, and is required.");
    if(endDate.size() == 0) throw runtime_error("Argument 'endDate' was empty, and is required.");

    // build the url string
    url += q; url += "command="; url += command;
    url += amp; url += "intervalInMinutes="; url += to_string(interval);
    url += amp; url += "startDateTime="; url += startDate;
    url += amp; url += "endDateTime="; url += endDate;
    url += amp; url += "username="; url += username;
    url += amp; url += "password="; url += password;

    // Get the data from the web-service
    string results = callWebService(url);
    JSONValue *data = JSON::Parse(results.c_str());

    // **********************************************************************
    // Load price data into v_fData
    if(data == NULL) {
        throw runtime_error("ERROR: Failed to parse results from server into JSON object.");
    } else {    // JSON object was successfully parsed
        JSONObject object = data->AsObject();
        JSONObject::const_iterator it = object.begin();
        while (it != object.end()) {
            wstring wsPrice = (*it).second->Stringify();
            string sPrice(wsPrice.begin(), wsPrice.end());
            sPrice.erase(sPrice.size() - 1);
            sPrice = sPrice.substr(sPrice.find('"') + 1);
            v_fData.push_back(atof(sPrice.c_str()));
            ++it;
        }
    }


    free(data);
    return v_fData;
}

fann_activationfunc_enum getActivationFunctionEnum(string name) {
	string fann_name;
	for(int i = FANN_LINEAR; i<=FANN_COS; i++) {
		fann_name = FANN_ACTIVATIONFUNC_NAMES[i];
		if(fann_name == name) {
			fann_activationfunc_enum foo = static_cast<fann_activationfunc_enum>(i);
			return foo;
		}
	}
	return FANN_GAUSSIAN_SYMMETRIC; // return the default if the name doesn't match.
}

fann_train_enum getTrainingFunctionEnum(string name) {
	string fann_name;
	for(int i = FANN_TRAIN_INCREMENTAL; i<=FANN_TRAIN_QUICKPROP; i++) {
		fann_name = FANN_TRAIN_NAMES[i];
		if(fann_name == name) {
			fann_train_enum foo = static_cast<fann_train_enum>(i);
			return foo;
		}
	}
	return FANN_TRAIN_BATCH;	// return the default if the name doesn't match.
}

string getDateTime(time_t iTime = 0, int web = 0) {
    string date;
    const time_t *t_ptr = &iTime;
    time_t t;
    if (iTime == 0) {
        t = time(NULL);
        t_ptr = &t;
    }

//    time_t t = time(&iTime);
    char mbstr[100];
    if(web) {
        if (strftime(mbstr, sizeof(mbstr), "%Y-%m-%d%%20%H:%M:%S", localtime(t_ptr))) {
            date = mbstr;
        }
    } else {
        if(strftime(mbstr, sizeof(mbstr), "%Y-%m-%d %I:%M:%S %p", localtime(t_ptr))) {
            date = mbstr;
        }
    }
    return date;
}

void transformInputFile(const string &tmp_input_filename, const string &test_filename, const string &test_data_filename, const string &train_data_file) {
	using namespace boost::filesystem;
	FANNData app;

//	string train_data_file;

	{
		const string orig_input_filename = g.data_dir + g.prediction_date + g.train_file_ext;
		copy_file(orig_input_filename.c_str(), tmp_input_filename.c_str(), copy_option::overwrite_if_exists);
	}

	/***** Extract and transform test file *****/
	if(app.extractTestSetFromInputFile(tmp_input_filename, test_filename, g.test_set_size) == g.test_set_size) {
		app.transformInputFile(test_filename, test_data_filename, g.delim);
	} else {
		string message = "Wrong number of data points in the test set: "+test_filename;
		throw message.c_str();
	}

	// TODO: COMPLETE: 1/30/2016 - the test data is successfully extracted from the train.csv file and transformed into the .DATA format, and saved to the filename 'test_data_filename'

	/***** Transform the training file from CSV to DATA *****/
	if(g.train_filename.empty()) {
		app.transformInputFile(tmp_input_filename, train_data_file, g.delim);
	} else {
		// train_data_file = g.data_dir+g.train_filename;
	}
}

bool train_ann(/*output*/ fann_type &mse_forest_bar) {
	// COMPLETED ON 1/22/2016: use boost to get list of all CSV files.  Write code that transforms each *train.csv file, launches the pedictor, save the prediction and appends the results to a CSV file.
	string network_filename = g.data_dir + g.prediction_date + g.network_ext;
	const string test_data_filename = g.data_dir + g.prediction_date + g.test_data_ext;
	g.train_filename = g.data_dir + g.prediction_date + g.train_data_ext;
	uint train_session = 1, epoch = 1;

	struct fann *ann = NULL;
	fann_train_data *test_data = NULL;

	// cout << "Training Started at " << getDateTime() << endl;
	// TODO: HIGH: Don't use a moving average, use a test set to determine if training is good enough.
	MovingAverage maFast(5); 		/*** the last 5 MSE values returned by fann ***/
	MovingAverage maSlow(15);
	fann_train_data *td = fann_read_train_from_file(g.train_filename.c_str());		// Get the training data from the .data file
//	bool training_successul = false;
	fann_type test_mse = 10000000.0f;
	fann_type mse_sessions_bar = mse_forest_bar;

	/***** Begin Training sessions loop *****/
	while (true) {
		// TODO: HIGH: 1/30/2016 - Test data set is now extracted and transformed.  Write code so it's used in the training session.
		/***** Re-init variables *****/
		g.training_successful = false;
		epoch = 1;
		/***** Create the ANN and set training parameters *****/
		uint ifile_columns =  g.getCountOfInputFileColumns();
//		cout << "Beginning Training Session: " << train_session << " of " << g.max_train_sessions << endl;
		// TODO: BACKLOG: Modify structure of the ANN's number of hidden layers, so that they can accomodate any numbrer of layers.
		if (ann)
			fann_destroy(ann);
		if (g.hidden_layers == 1)
			ann = fann_create_standard(1+g.hidden_layers+1, ifile_columns, int(ifile_columns*g.hidden_neurons_ratio), g.output_neurons);
		else if(g.hidden_layers == 2)
			ann = fann_create_standard(1+g.hidden_layers+1, ifile_columns, int(ifile_columns*g.hidden_neurons_ratio), int(ifile_columns*g.hidden_neurons_ratio), g.output_neurons);
		else if(g.hidden_layers == 3)
			ann = fann_create_standard(1+g.hidden_layers+1, ifile_columns, int(ifile_columns*g.hidden_neurons_ratio), int(ifile_columns*g.hidden_neurons_ratio), int(ifile_columns*g.hidden_neurons_ratio), g.output_neurons);

		fann_set_activation_function_hidden(ann, getActivationFunctionEnum(g.hidden_activation_function));
		fann_set_activation_function_output(ann, getActivationFunctionEnum(g.output_activation_function));
		fann_set_training_algorithm(ann, getTrainingFunctionEnum(g.training_function));			/*** Train methods are: _BATCH, _RPROP, _QUICKPROP, or write your own ***/
		test_data = fann_read_train_from_file(test_data_filename.c_str());  		/*** load test data 		***/

		/***** Begin Training Epochs loop *****/
		while (true){
			if(epoch > g.max_epochs) break;							/*** if max epochs reached, stop training ***/
			fann_set_learning_rate(ann, g.learning_rate);			/*** set the learning rate  ***/
			fann_train_epoch(ann, td);								/*** perform training epoch ***/
			test_mse = fann_test_data(ann, test_data);				/*** perform test epoch 	***/

			/*** output epoch number + addtl info 	***/
//			cout << "Session: " << train_session << " of " << g.max_train_sessions << "\tTraining Epoch: " << epoch << " of " << g.max_epochs << "\tTest MSE: " << test_mse << "\tLearning rate: " << fann_get_learning_rate(ann);
//			cout << "\tPredicting Date: "<<g.prediction_date << endl;

			/*** save the best_mse so we can output it later ***/
			if (test_mse < g.best_mse)
				g.best_mse = test_mse;

			/*** training was successful, break out of training epochs ***/
			if(test_mse <= mse_sessions_bar) {
				g.training_successful = true;
				break;
			}
			epoch++;
		} /*** end of TRAINING EPOCHS ***/

		/*** training was successful, break out of training sessions ***/
		if(g.training_successful) {
			fann_save(ann, network_filename.c_str());
			break;
		}

		/*** training wasn't successful, lower the bar a bit for next session ***/
		float train_progress = (float)train_session / (float)g.max_train_sessions;	// % of training sessions completed
		mse_sessions_bar = mse_sessions_bar + (g.best_mse - mse_sessions_bar) * train_progress;	// lower bar based on how many more training sessions we have

		/*** quit training, if we've reached the limit of training sessions ***/
		if(train_session >= g.max_train_sessions) {
			break;
		}

		train_session++;
	}	/*** end of TRAINING SESSIONS ***/

	if(!g.training_successful) {
		// cout << "****************************************************************************" << endl;
//		cout << "\t-----> The Padawan didn't complete his training.  The dark side has one, this time.  " << train_session << " out of "<< g.max_train_sessions << endl;
		// cout << "****************************************************************************" << endl;
		return false;
	}

//	cout << "\t-----> Training Completed at Training Session: " << train_session << " of " << g.max_train_sessions << endl;
	// // cout << "Desired MSE of test data: " << g.desired_mse << "\tActual Mean Squared Error Achieved by the ANN: " << test_mse << endl;

	/***** destroy heap resources *****/
    fann_destroy(ann);
//    // cout << "Training Completed at " << getDateTime() << endl;
    return true;
}

fann_type * normalizeASCII(char *data){
    // allocate new fann_type array large enough to hold data
    fann_type *retData = new fann_type[strlen(data)];

    // copy characters into fann_type array
    for (uint i = 0; i < strlen(data); i++) {
        //        retData[i] = (float)1/127 * (float)data[i];
        //        retData[i] = (float)(data[i] - (-1)) / (127 - (-1)); // normalize ascii characters between -1 and 1
        retData[i] = (float)data[i];
    }
    return retData;
}

bool isFileEmpty(const string & filename) {
	ifstream f (filename, istream::in);
	string line;
	if(f.is_open()){
		while (std::getline(f, line)) {
			if (line.find_first_not_of(
			  " \t\n\v\f\r" // whitespace
			  "\0\xFE\xFF"  // non-printing (used in various Unicode encodings)
			  ) != string::npos)
			  return false;
		}
	}
	f.close();
	return true;
}

void serializePrediction(string &filename, PredictionResult &p) {
	ofstream file (filename, ofstream::out);
	if(file.is_open()) {
		file << "Stock Symbol,Predicted Date,Prediction,Actual Price,Pct Diff From Actual,Date the prediction was made, Best MSE Achieved" << endl;
		file << p.symbol << "," << p.date << "," << fixed << setprecision(20) << p.prediction << ","<< fixed << setprecision(20) << p.actual <<","<<p.pct_diff_of_actual<<","<< p.date_prediction_made;
		file << ","<< fixed << setprecision(20) << p.best_mse << endl;
	}
	else
		throw "File '" + filename + "' couldn't be opened.";

	file.close();
	return;
}

PredictionResult makeAPrediction() {
	// TODO: HIGH: Make this function thread safe, by implementing std::shared_prt and std::thread
	PredictionResult p;
	// cout << "Predictions started at " << getDateTime() << endl;
	fann_type inputs[g.getCountOfInputFileColumns()];
	string network_file(g.data_dir + g.prediction_date + g.network_ext);
	string pifile(g.data_dir+g.prediction_date+g.predict_input_ext);
	FANNData fd;

	/*** If training failed, return an error instead of a prediction ***/
	if(g.training_successful){
        fd.saveInputsToFile(pifile, inputs, g.getCountOfInputFileColumns(), g.delim);
		fann_type thePrediction;

		// Load the trained network from its file
		struct fann *ann = fann_create_from_file(network_file.c_str());

		// Make the actual prediction, yes, finally!!
		thePrediction = *fann_run(ann, inputs);

		// Return the result
		p.prediction 			= thePrediction;
		p.mse					= fann_get_MSE(ann);		// TODO: HIGH: Super critical!  fix this line, just read it, it's easy.
		p.error 				= false;
		p.date 					= g.prediction_date;
		p.date_prediction_made 	= getDateTime();
		p.symbol 				= g.symbol;
		fann_destroy(ann);
	} else {
		/***** Store the values used to describe an error condition, i.e. the ANN didn't train successfully *****/
		p.error = true;
		p.prediction			= 0;
		p.mse					= 9999999999;
		// cout << "Training wasn't successful.  This prediction is no good.  :-(" << endl;
	}
    return p;
}

void getDesiredOutput(string &dofilename, PredictionResult &p) {
	FANNData fd;

	// Store the actual price and additional info about the prediction
    fd.saveInputsToFile(dofilename, &(p.actual), 1, g.delim);
	p.date = g.prediction_date;
	p.date_prediction_made = getDateTime();
	p.symbol = g.symbol;
	p.best_mse = g.best_mse;

	// calculate the percent difference between the predicted and actual price.
	if (!p.error) {
		p.pct_diff_of_actual = ((p.actual - p.prediction) / p.prediction) * 100;
	}

}

/**
 * Runs the predictor multiple times and returns the predictions
 *
 * @param forest_size - The number of predictions to make.
 * @param includeErrorsInResults - If true, include predictions that encountered an error condition during training.  If false, discard predictions with error condition.
 * @return vector<PredictionResult> - The predictions.  This will always have quantity equal to forest_size of predictions in it
 */
PredictionResultVector getForestOfPredictions(uint forest_size, bool includeErrorsInResults) {
	// TODO: HIGH: To increase performance, use heap memory and return a reference to a vector<PredictionResult> object.  Be sure to use std::smart_ptr for reference counting.
	string dofilename(g.data_dir+g.prediction_date+g.desired_out_ext);
	const string main_filename = g.data_dir + g.prediction_date + g.main_ext;
	const string train_data_filename = g.data_dir + g.prediction_date + g.train_data_ext;
	const string test_filename = g.data_dir + g.prediction_date + g.test_file_ext;
	const string test_data_filename = g.data_dir + g.prediction_date + g.test_data_ext;

	PredictionResultVector results;
	PredictionResult p;
	fann_type mse_goal = g.desired_mse;
	transformInputFile(main_filename, test_filename, test_data_filename, train_data_filename);
	for (uint i=0; i < forest_size; i++) {
//		cout << "Beginning prediction of tree: "<<i << " out of "<<forest_size << endl;
		if (train_ann(mse_goal)) {
			p = makeAPrediction();
			serializePrediction(g.lame_brain_filename, p);
			if (includeErrorsInResults) {
				getDesiredOutput(dofilename, p);
				results.push_back(p);
			} else if (!p.error) {
				getDesiredOutput(dofilename, p);
				results.push_back(p);
			}
		}
	}
	/*** cleanup tmp file ***/
//	remove(main_filename.c_str());
//	remove(test_filename.c_str());
//	remove(test_data_filename.c_str());
//	remove(train_data_filename.c_str());
	if (results.size() <= 0) {
		throw domain_error("A prediction couldn't be made using the selected input parameters.");
	}
	return results;
}

int run() {
	using namespace bad_ass;
	string actfilename(g.data_dir+g.prediction_date+g.predicted_output_ext);
	string predictorInputFilename = g.data_dir+g.prediction_date+g.predict_input_ext;
	string desiredOutputFilename = g.data_dir+g.prediction_date+g.desired_out_ext;

	/***** Error checking *****/
	if(isFileEmpty(predictorInputFilename)) {
		cout << "ERROR: the following file has nothing in it: "<< predictorInputFilename << endl;
		return 1;
	}
	if(isFileEmpty(desiredOutputFilename)) {
		cout << "ERROR: the following file has nothing in it: "<< desiredOutputFilename << endl;
		return 1;
	}

	/***** Run the predictor *****/
	PredictionResultVector results;
	PredictionResult p;
	try {
		results = getForestOfPredictions(g.forest_size, false);
		vector<fann_type> prices;
		if(g.standard_deviations != 0.0f) {
			// remove predictions greater than g.standard_deviations from the predicted prices
			prices = results.standardDeviationOfPrice(g.standard_deviations);
		} else {
			prices = results.getPredictions();
		}
		p = results.getPredictionObject();
		// use the average of predictions inside standard deviation threshold
		p.prediction = accumulate( prices.begin(), prices.end(), 0.0f) / prices.size();
	} catch (std::domain_error &e) {
		getDesiredOutput(desiredOutputFilename, p);
		p.error = true;
		p.best_mse = g.best_mse;
		cout << "ERROR: A prediction couldn't be made using the selected settings." << endl;
		serializePrediction(actfilename, p);
		return 1;
	}

	/***** Write the prediction data to the hard drive *****/
	getDesiredOutput(desiredOutputFilename, p);
//	cout << "Actual Price: " << p.actual << "\t\tPredicted Price: " << p.prediction << "\t\tError (%) = " << p.pct_diff_of_actual << endl;
	serializePrediction(actfilename, p);

//	cout << endl << "---------------------------------------------------------------------------------------------------------------" << endl << endl;
//	cout << setprecision(20);
//	cout << "The forest's prediction is..." << endl;
//	cout << "\t         Stock Symbol: " << p.symbol << endl;
//	cout << "\t       Date Predicted: " << p.date << endl;
//	cout << "\t         Actual Price: " << p.actual << endl;
//	cout << "\t      Predicted Price: " << p.prediction << endl;
//	cout << "\tPrediction's Accuracy: " << p.pct_diff_of_actual << endl;
//	cout << "\t    Best MSE Achieved: " << p.best_mse << endl;
//	cout << "\t      Number of Trees: " << results.size() << " out of " << g.forest_size << endl;
//	string err_str = (p.error == false) ? "Yes" : "No";
//	cout << "\t  Is Prediction Valid: " << err_str; // cout << endl;
//	cout << "\t   Prediction Created: " << p.date_prediction_made << endl;
//	cout << endl << "---------------------------------------------------------------------------------------------------------------" << endl;

	return 0;
}

}; 		// End of bad_ass namespace

int main(int argc, const char * argv[]) {
	using namespace bad_ass;
	namespace po = boost::program_options;
	bool interactive = false;

    // Declare the supported options.
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("data_dir", po::value<string>(), "The directory whereinput_neurons FANN will grab the training, input, and desired_output files from.")
		("prediction_date", po::value<string>(), "The predictor will attempt to predict this given date.  It must be given in the format: YY_MM_DD")
		("input_neurons", po::value<uint>(), "The number of inputs contained on each line of the training file.")
		("window_size", po::value<uint>(), "The size of the window contained on each line of the training file.  In other words, how many days of data the ANN will view at a single time.")
    	("hidden_layers", po::value<uint>(), "Specify the number of hidden layers that the predictor should use when making predictions.")
    	("hidden_activation_function", po::value<string>(), "Specify the activation function to be used in the ANN's hidden layers.  The following are valid values: \n"
    			 "FANN_LINEAR\n"
    			 "FANN_THRESHOLD\n"
    			 "FANN_THRESHOLD_SYMMETRIC\n"
    			 "FANN_SIGMOID\n"
    			 "FANN_SIGMOID_STEPWISE\n"
    			 "FANN_SIGMOID_SYMMETRIC\n"
    			 "FANN_SIGMOID_SYMMETRIC_STEPWISE\n"
    			 "FANN_GAUSSIAN\n"
    			 "FANN_GAUSSIAN_SYMMETRIC\n"
    			 "FANN_GAUSSIAN_STEPWISE\n"
    			 "FANN_ELLIOT\n"
    			 "FANN_ELLIOT_SYMMETRIC\n"
    			 "FANN_LINEAR_PIECE\n"
    			 "FANN_LINEAR_PIECE_SYMMETRIC\n"
    			 "FANN_SIN_SYMMETRIC\n"
    			 "FANN_COS_SYMMETRIC\n"
    			 "FANN_SIN\n"
    			 "FANN_COS")
    	("output_activation_function", po::value<string>(), "Specify the activation function to be used in the ANN's output layer.  The following are valid values: \n"
    			 "FANN_LINEAR\n"
    			 "FANN_THRESHOLD\n"
    			 "FANN_THRESHOLD_SYMMETRIC\n"
    			 "FANN_SIGMOID\n"
    			 "FANN_SIGMOID_STEPWISE\n"
    			 "FANN_SIGMOID_SYMMETRIC\n"
    			 "FANN_SIGMOID_SYMMETRIC_STEPWISE\n"
    			 "FANN_GAUSSIAN\n"
    			 "FANN_GAUSSIAN_SYMMETRIC\n"
    			 "FANN_GAUSSIAN_STEPWISE\n"
    			 "FANN_ELLIOT\n"
    			 "FANN_ELLIOT_SYMMETRIC\n"
    			 "FANN_LINEAR_PIECE\n"
    			 "FANN_LINEAR_PIECE_SYMMETRIC\n"
    			 "FANN_SIN_SYMMETRIC\n"
    			 "FANN_COS_SYMMETRIC\n"
    			 "FANN_SIN\n"
    			 "FANN_COS")
    	("desired_error", po::value<double>(), "Specify the desired error the ANN should attempt to reach before exiting training.")
    	("max_epochs", po::value<uint>(), "Specify the maximun number of training iterations (epochs) before the ANN exits training.")
		("delim", po::value<string>(), "Specify the delimeter character used in the training, input and desired output files.")
		("interactive", po::value<string>(), "Set this option to enable interactive mode.")
		("symbol", po::value<string>(), "Set the symbol the predictor will be predicting, so that it can be saved to the output file with the results.")
		("desired_mse", po::value<double>(), "Sets the target goal for the mean squared error, used during training.")
		("learning_rate", po::value<double>(), "Sets the learning rate of the ANN.  Default is 0.7, see fann_get_learning_rate() in FANN docs.")
		("max_train_sessions", po::value<double>(), "The max training sessions attempted to reach the desired error")
		("train_filename", po::value<string>(), "Set the training file name.  Overrides the default file name 'YY_MM_DD_train.csv' in the data directory.")
		("create_input_data_files", po::value<string>(), "If a value is specified, the input.data files will created, from the input.csv files.")
		("create_train_data_files", po::value<string>(), "If a value is specified, the train.csv files will be created, from the _input.csv files.")
		("standard_deviations", po::value<float>(), "Any predictions outside of this number of standard deviations will not be included in the final results.")
		("forest_size", po::value<uint>(), "Specify the number of predictions that should be when averaging the final result.")
    // TODO: HIGH: Add accept command line option to set g.training_function to FANN_TRAIN_BATCH, FANN_TRAIN_RPROP, FANN_TRAIN_QUICKPROP, or FANN_TRAIN_SARPROP
	;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
         cout << desc << "\n";
        return 1;
    }
    if (vm.count("data_dir")) {
    	g.data_dir = vm["data_dir"].as<string>();
//         cout << "option 1:  The CSV files will be retrieved from: "<< g.data_dir << endl;
    }
    if (vm.count("prediction_date")) {
    	g.prediction_date = vm["prediction_date"].as<string>();
//    	 cout << "option 2:  The date to be predicted will be: "<< g.prediction_date << endl;
	}
    if (vm.count("hidden_layers")) {
    	g.hidden_layers = vm["hidden_layers"].as<uint>();
//         cout << "option 3:  The number of hidden layers will be: "<< g.hidden_layers << endl;
    }
    if (vm.count("hidden_activation_function")) {
    	g.hidden_activation_function = vm["hidden_activation_function"].as<string>();
//         cout << "option 4:  The hidden layer's activiation function will be: "<< g.hidden_activation_function << endl;
    }
    if (vm.count("output_activation_function")) {
    	g.output_activation_function = vm["output_activation_function"].as<string>();
//         cout << "option 5:  The output layer's activation function will be: "<< g.output_activation_function << endl;
    }
    if (vm.count("desired_error")) {
    	g.desired_error = vm["desired_error"].as<double>();
//		 cout << "option 6:  The desired error will be: " << g.desired_error << endl;
    }
    if (vm.count("max_epochs")) {
        g.max_epochs = vm["max_epochs"].as<uint>();
//    	 cout << "option 7:  The max epochs used during training will be: "<< g.max_epochs << endl;
    }
    if (vm.count("delim")) {
    	g.delim = vm["delim"].as<string>();
//		 cout << "option 8:  The delimeter character in the CSV files is: "<< g.delim << endl;
    }
    if (vm.count("interactive")) {
		interactive = true;
//		 cout << "option 9:  Interactive mode is enabled." << endl;
	} else
//    	 cout << "option 9:  Interactive mode disabled." << endl;
    if (vm.count("input_neurons")) {
		g.input_neurons = vm["input_neurons"].as<uint>();
//		 cout << "option 10: The number of input neurons is: " << g.input_neurons << endl;
    }
    if (vm.count("window_size")) {
		g.window_size = vm["window_size"].as<uint>();
//		 cout << "option 11: The window is: " << g.window_size << endl;
	}
    if (vm.count("symbol")) {
		g.symbol = vm["symbol"].as<string>();
//		 cout << "option 12: The symbol we're predicting is: " << g.symbol << endl;
    }
    if (vm.count("desired_mse")) {
    	g.desired_mse = vm["desired_mse"].as<double>();
//    	 cout << "option 13: The mean squared error we're trying to get down to: " << g.desired_mse << endl;
    }
    if (vm.count("learning_rate")) {
    	g.learning_rate = vm["learning_rate"].as<double>();
//    	 cout << "option 14: The learning rate of the ANN is: " << g.learning_rate << endl;
    }
    if (vm.count("max_train_sessions")) {
    	g.max_train_sessions = vm["max_train_sessions"].as<double>();
//    	 cout << "option 15: The maximum number of training sessions is: " << g.max_train_sessions << endl;
    }
    if (vm.count("train_filename")) {
    	g.train_filename = vm["train_filename"].as<string>();
//    	 cout << "option 16: The training file name is: " << g.train_filename << endl;
    }
    if (vm.count("create_input_data_files")) {
//    	 cout << "option 17: Now creating the input.data files in: " << g.data_dir << endl;
    }
    if (vm.count("create_train_data_files")) {
    	// TODO: HIGH: 1/28/2016 - you might need to delete this code.  it might already be done using FANNData::extractTestSetFromInputFile()
//        cout << "option 14:  Now creating the train.data files in: " << g.data_dir << endl;
    	FANNData fd;
    	namespace fs = boost::filesystem;
    	vector<string> FileNames;
    	string out_filename;
		fs::path directory(g.data_dir.c_str());
		fs::directory_iterator files_in_data_dir(directory), end;
		/***** Iterate over all train.csv files, extracting the test sets *****/
		for(;files_in_data_dir != end; ++files_in_data_dir)
		{
			if (files_in_data_dir->path().extension() == ".csv") {
				string tf = files_in_data_dir->path().stem().string();
				if(tf.compare(10, 5, "train") == 0) {
					string trainFileName(files_in_data_dir->path().parent_path().string()+"/"+files_in_data_dir->path().filename().string());
					string testFileName(files_in_data_dir->path().parent_path().string()+"/"+g.prediction_date + "_test.csv");
//					trainFileName <<
					fd.extractTestSetFromInputFile(trainFileName, testFileName, 10); // extract 10 test data points
					fd.transformInputFile(trainFileName, testFileName, g.delim);
				}
			}
		}

    }
    if (vm.count("standard_deviations")) {
    	g.standard_deviations = vm["standard_deviations"].as<float>();
//    	 cout << "option 15:  Standard deviations are: " << g.standard_deviations << endl;

    }
    if (vm.count("forest_size")) {
    	g.forest_size = vm["forest_size"].as<uint>();
//    	 cout << "option 16:  Now creating the input.data files in: " << g.forest_size << endl;

    }

    if (!interactive) {
    	run();
//    	 cout << "Predictor is exiting at " << getDateTime() << endl;
    	return 0;
    }

    char command = 'b';
    while(command != 'Q') {
    	 cout << endl << "Enter [r] to run the predictor." << endl;
         cout << "Enter [q] to quit." << endl;
        cin >> command;
        command = toupper(command);

        if (command == 'R') {
        	run();
        } else if(command == 'Q') {
             cout << "Thank you for using the bad ass Mon$y maker!" << endl;
            break;
        } else {
             cout << "You've entered an invalid key.  Please try again." << endl;
        }
    }
    return 0;
}

// COMPLETE ON 1/19/2016: Create a function that transforms CSV data from a file and saves it to a new file using the .data format for FANN.
// 				See this link for an example: http://leenissen.dk/fann/html/files2/gettingstarted-txt.html






