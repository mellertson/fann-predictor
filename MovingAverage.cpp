/*
 * MovingAverage.cpp
 *
 *  Created on: Jan 25, 2016
 *      Author: mellertson
 */

#include "MovingAverage.h"

namespace std {

MovingAverage::MovingAverage(int size) {
	for(int i=0; i<size; i++){
		ma.push_back(0);
	}
}

MovingAverage::~MovingAverage() {

}

void MovingAverage::addValue(float value) {
	ma.pop_front();
	ma.push_back(value);
}

float MovingAverage::getMovingAverage() {
	float result = 0;
	for(list<float>::iterator it=ma.begin(); it != ma.end(); ++it) {
		result = result + *it;
	}
	result = result / ma.size();
	return result;
}

} /* namespace std */
