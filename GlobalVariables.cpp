/*
 * GlobalVariables.cpp
 *
 *  Created on: Jan 21, 2016
 *      Author: mellertson
 */

#include "GlobalVariables.h"

namespace bad_ass {

uint GlobalVariables::getCountOfInputFileColumns() {
	return input_neurons * window_size;
}

} /* namespace fann_predictor */
