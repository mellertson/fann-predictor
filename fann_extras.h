/*
 * fann_extras.h
 *
 *  Created on: Dec 29, 2015
 *      Author: mellertson
 */

#ifndef __fann_predict__fann_extras__
#define __fann_predict__fann_extras__

#include <stdio.h>
#include <fann.h>

int fann_set_train_data(struct fann_train_data* data, unsigned int num, fann_type* input, fann_type* output);
int fann_get_train_data(struct fann_train_data* data, unsigned int num, fann_type* input, fann_type* output);


#endif /* defined(__fann_predict__fann_extras__) */
