/*
 * GlobalVariables.h
 *
 *  Created on: Jan 21, 2016
 *      Author: mellertson
 */

#ifndef GLOBALVARIABLES_H_
#define GLOBALVARIABLES_H_

#include <string>
#include <fann.h>

namespace bad_ass {

using namespace std;

class GlobalVariables {
public:
	string 	data_dir 					= "/home/mellertson/Desktop/Junk/data_tmp2/";
	string 	train_filename				= "";
	const string main_ext				= "_main.csv";
	const string train_file_ext			= "_train.csv";
	const string train_data_ext			= "_train.data";
	const string test_file_ext			= "_test.csv";
	const string test_data_ext			= "_test.data";
	const string predict_input_ext		= "_predictor_input.csv";
	const string predicted_output_ext	= "_predicted_output.csv";
	const string desired_out_ext		= "_desired_output.csv";
	const string network_ext			= "_predict.net";
	string lame_brain_filename			= "gene_and_james_show.csv";
	bool training_successful			= false;
	uint	test_set_size				= 5;
	string 	prediction_date 			= "16_01_04";
	uint 	input_neurons				= 1;
	float	hidden_neurons_ratio		= 1.5;
	uint 	output_neurons				= 1;
	uint 	window_size					= 1;
	uint 	hidden_layers 				= 1;
	string 	hidden_activation_function 	= "FANN_GAUSSIAN_SYMMETRIC";
	string 	output_activation_function 	= "FANN_LINEAR";
	string  training_function			= "FANN_TRAIN_RPROP";
	double 	desired_error 				= 0.00025;	// MSI output by fann during training
	double 	desired_mse					= 0.0016;	// MSE from test data points
	float	learning_rate				= 0.7;		// Default is 0.7, see fann_get_learning_rate() in fann docs
	uint 	max_train_sessions			= 100;
	uint 	max_epochs 					= 200;
	string 	delim 						= ",";
	string 	symbol						= "Not provided by calling function";
	float	standard_deviations			= 2.0f;
	uint 	forest_size					= 1;
	fann_type	best_mse				= 1000000.0f;		// used to improve training results after a failed training session.

	/*** global helper functions ***/
	uint getCountOfInputFileColumns();
};

} /* namespace fann_predictor */

#endif /* GLOBALVARIABLES_H_ */
