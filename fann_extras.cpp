/*
 * fann_extras.cpp
 *
 *  Created on: Dec 29, 2015
 *      Author: mellertson
 */

#include "fann_extras.h"


/*
 * Sets the input and desired output values into the specified position in the training data structure
 */
int fann_set_train_data(struct fann_train_data* data, unsigned int num, fann_type* input, fann_type* output)
{
    unsigned int i;

    if(num>=data->num_data)
    {
        fann_error(NULL, FANN_E_INDEX_OUT_OF_BOUND);
        return -1;
    }

    for(i=0; i < data->num_input;  ++i) data->input[num][i] = input[i];
    for(i=0; i < data->num_output; ++i) data->output[num][i] = output[i];

    return 0;
}


/*
 * Gets the input and desired output values from the specified position in the training data structure
 */
int fann_get_train_data(struct fann_train_data* data, unsigned int num, fann_type* input, fann_type* output)
{
    unsigned int i;

    if(num>=data->num_data)
    {
        fann_error(NULL, FANN_E_INDEX_OUT_OF_BOUND);
        return -1;
    }

    for(i=0;i<data->num_input;++i) input[i] = data->input[num][i];
    for(i=0;i<data->num_output;++i) output[i] = data->output[num][i];

    return 0;
}


