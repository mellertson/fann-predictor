/*
 * FANNData.h
 *
 *  Created on: Jan 19, 2016
 *      Author: mellertson
 */

#ifndef FANNDATA_H_
#define FANNDATA_H_

#include <string>
#include <vector>
#include <fann.h>
#include "PredictionResults.h"

namespace bad_ass {
using namespace std;


class FANNData {
private:
	uint annShape[3];
	std::vector<std::vector<double> > data;
public:
	FANNData();
	virtual ~FANNData();
	void transformInputFile(const string &filename, const string &outfilename, const string &delimeter);
	void saveInputsToFile(const string &filename, fann_type *inputs, uint inputsCount, string &delimeter);
	void writeFannTypeToFile(const string &filename, fann_type *value);
	uint extractTestSetFromInputFile(const string &in_filename, const string &out_filename, uint n);
	uint getFileLineCount(const string &filename);
	static vector<PredictionResult> standardDeviation(vector <PredictionResult> data, uint num_deviations);
};

}

#endif /* FANNDATA_H_ */
