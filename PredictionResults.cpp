/*
 * PredictionResultVector.cpp
 *
 *  Created on: Feb 9, 2016
 *      Author: mellertson
 */

#include "PredictionResults.h"

namespace bad_ass {

PredictionResultVector::PredictionResultVector() {

}

PredictionResultVector::~PredictionResultVector() {

}

/**
 * Calculates the standard deviation of the predictions contained in this object, and returns them after removing any data with a standard deviation greater than the value given in 'num_deviations'
 * @param num_deviations - Default = 2.0
 * @return vector<fann_type> - All the prediction values, after removing any standard devitions greater than 'num_deviations'
 */
vector<fann_type> PredictionResultVector::standardDeviationOfPrice(uint num_deviations = 2.0) {
	vector <fann_type> prices = this->getPredictions();
	float mean = accumulate( prices.begin(), prices.end(), 0.0f )/ prices.size();
	vector<fann_type> zero_mean( prices );
	transform( zero_mean.begin(), zero_mean.end(), zero_mean.begin(),bind2nd( minus<float>(), mean ) );

	// compute the sample standard deviation
	float deviation = inner_product( zero_mean.begin(),zero_mean.end(), zero_mean.begin(), 0.0f );
	deviation = sqrt( deviation / ( prices.size() - 1 ) );

	// erase all points more than three standard deviations greater than the mean
	vector<float>::iterator end = remove_if( prices.begin(), prices.end(), bind2nd( greater<float>(),mean + num_deviations * deviation ) );
	prices.erase( end, prices.end() );
	return prices;
}

vector<fann_type> PredictionResultVector::getPredictions() {
	vector <fann_type> prices;
	vector<PredictionResult>::iterator it;
	for(it = this->begin(); it != this->end(); it++) {
		prices.push_back(it->prediction);
	}
	return prices;
}

/**
 * Gets the 1st PredictionResult stored in this vector.
 * @return PredictionResult
 * @throws std::domain_error if this vector is empty.
 */
PredictionResult PredictionResultVector::getPredictionObject() {
	if (this->size() <= 0)
		throw domain_error("There are no predictions in this vector.");
	return this->operator [](0);
}

} /* namespace bad_ass */
